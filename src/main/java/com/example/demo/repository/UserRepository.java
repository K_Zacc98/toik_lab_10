package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public boolean checkLogin(final String login, final String password) {
        // TODO: Prosze dokonczyc implementacje...

        for (int iter = 1; iter < usersDatabase.size() + 1; iter++) {
            if (usersDatabase.get(iter).getLogin().equals(login) && usersDatabase.get(iter).getPassword().equals(password)) {
                return true;
            } else {
                usersDatabase.get(iter).setIncorrectLoginCounter(usersDatabase.get(iter).getIncorrectLoginCounter() + 1);
            }
            if (usersDatabase.get(iter).getIncorrectLoginCounter() == 3) {
                usersDatabase.get(iter).setActive(false);
            }
        }
        return false;
    }

    public boolean checkActive(final String login) {
        for (int iter = 1; iter < usersDatabase.size() + 1; iter++) {
            if (usersDatabase.get(iter).getLogin().equals(login)) {
                return usersDatabase.get(iter).isActive();
            }
        }
        return true;
    }
}
